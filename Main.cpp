
#include <iostream>


class Vector
{
public:

	Vector() : x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	void Show()
	{

		std::cout << "\n" << x << " " << y << " " << z << "\n";

	}
	void VectorMod()
	{

		double m = sqrt(x*x + y*y + z*z);

		std::cout << "\n" << "Vector modulus = " << m << "\n" << "\n";
	}

private:
	double x, y, z;
};


int main()
{

	double a, b, c;

	std::cout << "Enter x - ";
	std::cin >> a;
	std::cout << "Enter y - ";
	std::cin >> b;
	std::cout << "Enter z - ";
	std::cin >> c;


	Vector v(a, b, c);
	v.Show();
	v.VectorMod();
}

